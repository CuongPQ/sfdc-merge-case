<apex:page docType="html-5.0" sidebar="false" title="Batch Export - Duplicate Check for Salesforce" controller="dupcheck.dc3ControllerBatchExport" action="{!janitor}" tabstyle="dcBatch__tab">
 
  <apex:stylesheet value="{!URLFOR($Resource.dupcheck__slds, '/assets/styles/salesforce-lightning-design-system-vf.min.css')}" />
  <apex:stylesheet value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/style/dupcheckApp.css" />
  
  <apex:includeScript value="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" />
  <apex:includeScript value="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js" />
  <apex:includeScript value="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-sanitize.min.js" />
  
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/plauti-ng-slds.min.js" />
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/angular-csv.min.js" />
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/dupcheck.js" />
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/dupcheck.ng.directives.js" />
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/dupcheck.ng.services.js" />
  <apex:includeScript value="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheck__dupcheckAssets,'/dupcheckAssets'))}/lightning/js/dupcheck.ng.batch.export.js" />   
      
      
  <div id="dataStore" class="slds" ng-app="dcApp" ng-controller="batchExportController" data-job-id="{!jobId}" data-sf-get-data="{!$RemoteAction.dc3ControllerBatchExport.getData}">
  
    <div ng-if="{!NOT(license.features.batchExport)}">
      <div class="slds-page-header" role="banner">
        <div class="slds-grid slds-wrap">
          <div class="slds-col slds-has-flexi-truncate ">
            <div class="slds-media">
              <div class="slds-media__figure">
                <img class="slds-icon slds-icon--large slds-icon-action-close" ng-src="{!URLFOR($Resource.slds,'')}/assets/icons/action/close_120.png" />
              </div>
              <div class="slds-media__body">
                <p class="slds-text-heading--label">Error</p>
                <div class="slds-grid">
                  <h1 class="slds-text-heading--medium slds-m-right--small slds-truncate slds-align-middle" title="Record Title">No Access</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="slds-grid slds-wrap slds-m-top--medium" ng-if="{!NOT(license.features.batchExport)}">
        <div class="slds-col--padded slds-size--1-of-1">
          <div class="slds-card slds-card--empty slds-m-bottom--x-large">
            <div class="slds-card__body slds-p-horizontal--small">
              <h3 class="slds-text-heading--small slds-p-top--large slds-p-bottom--large">You're not licensed to use this feature.</h3>
              <button class="slds-button slds-button--neutral slds-button--small slds-m-bottom--x-large" onClick="dcNavigate('{!$Page.dc3License}')">
                Show License
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>  
  
  
  
    <div ng-if="{!license.features.batchExport}">
      <div class="slds-page-header" role="banner">
          <div class="slds-grid slds-wrap">
            <div class="slds-col slds-size--1-of-1 slds-medium-size--4-of-6 slds-large-size--4-of-6">
                      <div class="slds-media">
                          <div class="slds-media__figure">
                              <img class="slds-icon slds-icon--large" ng-src="{!IF(isDev,AssetsUrl,URLFOR($Resource.dupcheckAssets,'/dupcheckAssets'))}/lightning/app/app_icon_48.png" />
                          </div> 
                          <div class="slds-media__body">
                              <p class="slds-text-heading--label">Duplicate Check</p>
                              <div class="slds-grid">
                                  <h1 class="slds-text-heading--medium slds-m-right--small slds-truncate slds-align-middle">Export&nbsp;<apex:outputText value="{!job.jobName}"/></h1>
                              </div>
                          </div>
                      </div>
                  </div>
              <div class="slds-col slds-size--1-of-1 slds-medium-size--2-of-6 slds-large-size--2-of-6 ">
                  <div class="slds-grid slds-float--right"> 
              
              <button class="slds-button slds-button--icon-border-filled" onClick="dcNavigate('{!$Page.dc3Batch}')">
                <img class="slds-button__icon" ng-src="{!URLFOR($Resource.slds,'')}/assets/icons/utility/back_120.png" />
              </button>
              
              <button class="slds-button slds-button--icon-border-filled" ng-click="openUrl(meta.helpPage)">
                          <img class="slds-button__icon" ng-src="{!URLFOR($Resource.slds,'')}/assets/icons/utility/help_120.png" />
              </button>
                            
                    </div>
                </div>
            </div>
      </div>
    </div> 
    
    <div class="slds-grid slds-wrap">
    
      <div class="slds-col--padded">
      
        <div class="slds-form-element slds-m-top--large slds-m-bottom--medium slds-grid slds-grid--align-spread">
          <div class="slds-form-element__control slds-has-divider--bottom">
            <span class="slds-form-element__static" ng-if="!meta.isLoading" ng-bind-html="meta.text"></span>
            <span class="slds-form-element__static" ng-if="meta.isLoading">Fetching records...</span>
          </div>
        </div>  
      
        <div class="slds-m-top--medium slds-m-bottom--medium">
          <button class="slds-button slds-button--neutral" ng-csv="data.dataList" csv-header="['Group', 'Pair Id', 'Record A Id', 'Record B Id', 'Record A Name', 'Record B Name', 'Score']" quote-strings="'" filename="{!job.jobName} export.csv" ng-disabled="meta.isLoading && data.dataList.length > 0">Download CSV</button>
          <img class="slds-button__icon slds-button__icon--large" ng-if="meta.isLoading" src="{!URLFOR($Resource.slds,'')}/assets/images/spinners/slds_spinner_brand.gif" />   
        </div>                              
        
      </div>
    
    </div>
    
    
    
    
    
  </div>
  
  

</apex:page>