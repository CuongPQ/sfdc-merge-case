<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Used to capture information related to deferred revenue and expenses.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account2__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The account associated with the scheduled revenue expense record</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Scheduled Revenue Expenses</relationshipLabel>
        <relationshipName>Scheduled_Revenue_Expenses</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account_Payable__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference to a related Account Payable if using scheduled expenses.</inlineHelpText>
        <label>Account Payable</label>
        <referenceTo>Account_Payable__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Account2__c),Project__r.Account__r.Name,Account2__r.Name)</formula>
        <inlineHelpText>The account on the project associated with this record.</inlineHelpText>
        <label>Project Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Accounting_Period__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Used to specify accounting period for financial reporting. Accounting period must have a value of Open.</inlineHelpText>
        <label>Accounting  Period</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Accounting_Period__c.Status__c</field>
                <operation>equals</operation>
                <value>Open</value>
            </filterItems>
            <infoMessage>The accounting period status must be open</infoMessage>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Accounting_Period__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Amount of Scheduled Revenue or Expense. Currency field. Values should be positive.</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billing__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference to a related Billing if using Scheduled Revenue.</inlineHelpText>
        <label>Billing</label>
        <referenceTo>Billing__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Credit_GL_Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit GL Account</label>
        <referenceTo>GL_Account__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (Credit GL Account)</relationshipLabel>
        <relationshipName>Scheduled_Revenue_Expense1</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Debit_GL_Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Debit GL Account</label>
        <referenceTo>GL_Account__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (Debit GL Account)</relationshipLabel>
        <relationshipName>Scheduled_Revenue_Expense</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Brief description of the scheduled revenue or expense to help manage selections for sorting and posting.</inlineHelpText>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fixed_Asset__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fixed Asset</label>
        <referenceTo>Fixed_Asset__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expenses</relationshipLabel>
        <relationshipName>Scheduled_Revenue_Expenses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GL_Account_Variable_1__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.</inlineHelpText>
        <label>GL Account Variable 1</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Accounting Variable must have a Type of &quot;GL Account Variable 1&quot;.</errorMessage>
            <filterItems>
                <field>Accounting_Variable__c.Type__c</field>
                <operation>equals</operation>
                <value>GL Account Variable 1</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Accounting_Variable__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (GL Account Variable 1)</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GL_Account_Variable_2__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.</inlineHelpText>
        <label>GL Account Variable 2</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Accounting Variable must have a Type of &quot;GL Account Variable 2&quot;.</errorMessage>
            <filterItems>
                <field>Accounting_Variable__c.Type__c</field>
                <operation>equals</operation>
                <value>GL Account Variable 2</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Accounting_Variable__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (GL Account Variable 2)</relationshipLabel>
        <relationshipName>Scheduled_Revenues1</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GL_Account_Variable_3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.</inlineHelpText>
        <label>GL Account Variable 3</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The Accounting Variable must have a Type of &quot;GL Account Variable 3&quot;.</errorMessage>
            <filterItems>
                <field>Accounting_Variable__c.Type__c</field>
                <operation>equals</operation>
                <value>GL Account Variable 3</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Accounting_Variable__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (GL Account Variable 3)</relationshipLabel>
        <relationshipName>Scheduled_Revenues2</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GL_Account_Variable_4__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Traditional Sub-General Ledger account segment used to report on divisions, departments, geographies, cost centers, profit centers or business units.</inlineHelpText>
        <label>GL Account Variable 4</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Accounting_Variable__c.Type__c</field>
                <operation>equals</operation>
                <value>GL Account Variable 4</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Accounting_Variable__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense (GL Account Variable 4)</relationshipLabel>
        <relationshipName>Scheduled_Revenues3</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenue_Expense</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project_Task__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Work break down structure for projects, such as: Phases, Deliverables or Roles.</inlineHelpText>
        <label>Project Task</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Project__c</field>
                <operation>equals</operation>
                <valueField>Project_Task__c.Project__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Project_Task__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Related Project allocation of Cost or Revenue.</inlineHelpText>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Scheduled Revenue Expense</relationshipLabel>
        <relationshipName>Scheduled_Revenues</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Scheduled indicates the record has not been processed yet. A value of &quot;Posted&quot; indicates the record is final and a debit and credit line have been entered into the transaction object for this revenue or expense.</inlineHelpText>
        <label>Posting Status</label>
        <picklist>
            <picklistValues>
                <fullName>Scheduled</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Scheduled Revenue Expense</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Accounting_Period__c</columns>
        <columns>Status__c</columns>
        <columns>Amount__c</columns>
        <columns>Description__c</columns>
        <columns>Debit_GL_Account__c</columns>
        <columns>Credit_GL_Account__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Sched Rev Exp Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Scheduled Revenue Expenses</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Accounting_Period__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Credit_GL_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Debit_GL_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Amount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <listViewButtons>Post</listViewButtons>
        <listViewButtons>Unpost</listViewButtons>
        <lookupDialogsAdditionalFields>Accounting_Period__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Credit_GL_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Debit_GL_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>Accounting_Period__c</lookupFilterFields>
        <lookupFilterFields>Credit_GL_Account__c</lookupFilterFields>
        <lookupFilterFields>Debit_GL_Account__c</lookupFilterFields>
        <lookupFilterFields>Amount__c</lookupFilterFields>
        <lookupFilterFields>Status__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Accounting_Period__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Credit_GL_Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Debit_GL_Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Amount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Accounting_Period__c</searchFilterFields>
        <searchFilterFields>Credit_GL_Account__c</searchFilterFields>
        <searchFilterFields>Debit_GL_Account__c</searchFilterFields>
        <searchFilterFields>Amount__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Description__c</searchFilterFields>
        <searchResultsAdditionalFields>Accounting_Period__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Credit_GL_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Debit_GL_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Amount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Amount_Must_be_Positive</fullName>
        <active>true</active>
        <description>Amount Must be positive.</description>
        <errorConditionFormula>Amount__c &lt;0</errorConditionFormula>
        <errorDisplayField>Amount__c</errorDisplayField>
        <errorMessage>The amount must be positive.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Project_Task_Required_With_Project</fullName>
        <active>true</active>
        <description>Rule ensures a Project Task is Required if the Project field is used.</description>
        <errorConditionFormula>NOT(ISBLANK(Project__c)) &amp;&amp; ISBLANK(Project_Task__c)</errorConditionFormula>
        <errorDisplayField>Project_Task__c</errorDisplayField>
        <errorMessage>A Project Task is required if a Project is selected.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Value_for_Accounting_Period</fullName>
        <active>true</active>
        <description>Verifies accounting period field is not blank.</description>
        <errorConditionFormula>ISBLANK( Accounting_Period__c )</errorConditionFormula>
        <errorMessage>A scheduled revenue or expense must contain a value for the accounting period field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Value_for_Credit_GL_Account</fullName>
        <active>true</active>
        <description>Verifies credit gl account field is not blank.</description>
        <errorConditionFormula>ISBLANK( Credit_GL_Account__c )</errorConditionFormula>
        <errorMessage>A scheduled revenue or expense must contain a value for the credit gl account field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Value_for_Debit_GL_Account</fullName>
        <active>true</active>
        <description>Verifies debit gl account field is not blank.</description>
        <errorConditionFormula>ISBLANK( Debit_GL_Account__c )</errorConditionFormula>
        <errorMessage>A scheduled revenue or expense must contain a value for the debit gl account field.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Post</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Post</masterLabel>
        <openType>sidebar</openType>
        <page>ScheduledRevenueExpenseBatchPost</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>Post_Detail</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Post</masterLabel>
        <openType>sidebar</openType>
        <page>ScheduledRevenueExpensePostTransaction</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Unpost</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Unpost</masterLabel>
        <openType>sidebar</openType>
        <page>ScheduledRevenueExpenseBatchUnpost</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>Unpost_Detail</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Unpost</masterLabel>
        <openType>sidebar</openType>
        <page>ScheduledRevenueExpenseUnpost</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
