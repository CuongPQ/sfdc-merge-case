<apex:page standardController="AcctSeed__Bank_Reconciliation__c" extensions="AcctSeed.BankReconciliationClearTransactions" id="thePage">
	<style type="text/css">
		.table_footer {
			background-color:#E0E0E0;
			font-weight:bold;
		}
	</style>
	<apex:form id="theForm">
		<apex:pageBlock id="thePageBlock" title="Bank Reconciliation">
			<apex:pageMessages />
			<apex:pageBlockButtons location="bottom">
				<apex:actionStatus id="mySaveStatus" rendered="{!isSaveDisplay}">
					<apex:facet name="stop">
						<apex:commandButton action="{!saveTransactions}" value="Commit" status="mySaveStatus" rerender="thePageBlock,results" />
					</apex:facet>
					<apex:facet name="start">
						<apex:commandButton action="{!saveTransactions}" value="Committing..." disabled="true" />
					</apex:facet>
				</apex:actionStatus>
				<apex:commandButton action="/{!Bank_Reconciliation__c.Id}" value="Back" immediate="true"/>
				<apex:actionStatus id="myUpdateView">
					<apex:facet name="stop">
						<apex:commandButton action="{!getTransactions}" value="Update View" status="myUpdateView" rerender="thePageBlock,results" />
					</apex:facet>
					<apex:facet name="start">
						<apex:commandButton action="{!getTransactions}" value="Processing..." disabled="true" />
					</apex:facet>
				</apex:actionStatus>
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="2">
				<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Ledger_Balance__c}"/>
				<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Reconciliation_Date__c}"/>
				<apex:outputField value="{!bankRec.AcctSeed__Plus_Outstanding_Disbursements__c}"/>
				<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__GL_Account__c}"/>
				<apex:outputField value="{!bankRec.AcctSeed__Minus_Outstanding_Deposits__c}"/>
				<apex:inputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Start_Date__c}" required="true"/>
				<apex:outputField value="{!bankRec.AcctSeed__Bank_Adjustments__c}"/>
				<apex:inputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__End_Date__c}" required="true"/>
				<apex:outputField value="{!bankRec.AcctSeed__Calculated_Bank_Balance__c}"/>	
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Type"/>
					<apex:selectList value="{!selectedType}" size="1">
						<apex:selectOptions value="{!type}"/>
					</apex:selectList>
				</apex:pageBlockSectionItem>				
				<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Bank_Balance__c}"/>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Subtotal By Day"/>
					<apex:inputCheckbox value="{!isSubTotalByDay}"/>
				</apex:pageBlockSectionItem>			
				<apex:outputField value="{!bankRec.AcctSeed__Difference__c}"/>	
				<apex:outputField value="{!bankRec.AcctSeed__Cleared_Disbursements__c}"/>
				<!-- apex:pageBlockSectionItem >
					<apex:outputLabel value="Reconciliation Status"/>
					<apex:selectList value="{!selectedStatus}" size="1">
						<apex:selectOptions value="{!status}"/>
					</apex:selectList>
				</apex:pageBlockSectionItem -->
				<apex:pageBlockSectionItem />
				<apex:outputField value="{!bankRec.AcctSeed__Cleared_Deposits__c}"/>
			</apex:pageBlockSection>
		</apex:pageBlock>
		
		<apex:outputPanel id="results">
			<apex:pageBlock id="pageBlockNoResults" rendered="{!isNoResults}">
				<div class="errorStyle">No results were returned for the criteria provided. Please change the criteria and try again.</div>
			</apex:pageBlock>
			<apex:pageBlock id="pageBlockResults" rendered="{!transactionSectionWrapperList.size>0}">
				<apex:variable var="linkCount" value="{!1}" />
				<apex:repeat value="{!transactionSectionWrapperList}" var="section">
					<apex:outputPanel id="links" rendered="{!linkCount=1}">
						<apex:commandLink value="All Cleared" action="{!setAllCleared}" rerender="results" title="Click to mark all transactions as cleared"/>&nbsp;&nbsp;&nbsp;<apex:commandLink value="All Outstanding" action="{!setAllOutstanding}" rerender="results" title="Click to mark all transactions as outstanding"/><br/><br/>
					</apex:outputPanel>
					<apex:pageBlockTable value="{!section.transactions}" var="item" footerClass="table_footer">				
						<apex:column width="15%">
							<apex:inputField value="{!item.bankLine.AcctSeed__Bank_Reconciliation_Status__c}" required="true"/>
							<apex:facet name="header">
								Reconciliation Status
							</apex:facet>
						</apex:column>
						<apex:column headerValue="Transaction" width="15%">
							<apex:outputLink target="_blank" value="/{!item.trans.Id}" rendered="{!NOT(ISBLANK(item.trans.Name))}">{!item.trans.Name}</apex:outputLink>
						</apex:column>
						<apex:column value="{!item.trans.AcctSeed__Date__c}" width="15%"/>
						<apex:column value="{!item.trans.Accounting_Period__r.Name}" width="15%"/>
						<apex:column headerValue="Source" width="15%">
							<apex:outputLink target="_blank" value="/{!item.sourceId}" rendered="{!NOT(ISBLANK(item.sourceName))}">{!item.sourceName}</apex:outputLink>
						</apex:column>
						<apex:column headerValue="Reference" value="{!item.reference}" width="15%"/>
						<apex:column value="{!item.trans.AcctSeed__Amount__c}" width="15%">
							<apex:facet name="footer">
								<apex:outputText value="{0, number, $###,###,##0.00}">
									<apex:param value="{!section.total}"/>
								</apex:outputText>
							</apex:facet>
						</apex:column>				
					</apex:pageBlockTable>
					<br/>
					<apex:variable var="linkCount" value="{!linkCount+1}" />
				</apex:repeat>	
			</apex:pageBlock>
		</apex:outputPanel>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Bank_Adjustments__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Minus_Outstanding_Deposits__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Plus_Outstanding_Disbursements__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Cleared_Disbursements__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Cleared_Deposits__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__GL_Account__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Calculated_Bank_Balance__c}" rendered="false"/>
		<apex:outputField value="{!AcctSeed__Bank_Reconciliation__c.AcctSeed__Difference__c}" rendered="false"/>
	</apex:form>
</apex:page>