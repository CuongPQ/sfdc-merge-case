<apex:page id="thePage" standardController="AcctSeed__Cash_Receipt__c" applyBodyTag="false" extensions="AcctSeed.CashReceiptApplyBillings" sidebar="false">
    <apex:stylesheet value="{!URLFOR($Resource.AcctSeed__accounting_resources, '/css/cash-apply.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.AcctSeed__accounting_resources, '/javascript/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.AcctSeed__accounting_resources, '/javascript/numeral.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.AcctSeed__accounting_resources, '/javascript/accounting-common.js')}"/>   
    <apex:includeScript value="{!URLFOR($Resource.AcctSeed__accounting_resources, '/javascript/cash-apply.js')}"/>       
    <apex:form id="theForm" onkeypress="return AcctSeed.ASModule.noenter(event);">
        <apex:sectionHeader title="{!$ObjectType.AcctSeed__Cash_Receipt__c.label}" subtitle="{!AcctSeed__Cash_Receipt__c.Name}" />
        <apex:pageBlock id="thePageBlock" title="{!$ObjectType.AcctSeed__Cash_Receipt__c.label} Apply">
            <apex:pageMessages id="thePageMessages" escape="false" />
            <apex:pageMessage summary="The {!$ObjectType.AcctSeed__Cash_Receipt__c.label} must be posted to apply to {!$ObjectType.AcctSeed__Billing__c.labelPlural}" severity="error" strength="3" rendered="{!AcctSeed__Cash_Receipt__c.AcctSeed__Status__c!='Posted'}" />
            <apex:pageMessage summary="The cash receipt does not have any matching billing records for {!AcctSeed__Cash_Receipt__c.Account__r.Name}" severity="error" strength="3" rendered="{!AcctSeed__Cash_Receipt__c.AcctSeed__Status__c='Posted' && billCashWrapperList.size=0}" />                
            <apex:pageBlockButtons id="buttons">
                <apex:actionStatus id="saveStatus" onstart="AcctSeed.ASModule.disableButtons('Processing...');AcctSeed.ASModule.disableArrowButtons();" />
                <apex:commandButton id="saveButton" action="{!save}" value="Save" status="saveStatus" reRender="theForm" rendered="{!NOT(isSaved) && AcctSeed__Cash_Receipt__c.AcctSeed__Status__c="Posted" && !isLastModifiedError && billCashWrapperList.size>0}" />
                <apex:commandButton id="cancelButton" action="{!cancel}" value="Back" immediate="true" />
                <apex:commandButton action="/{!$ObjectType.Cash_Receipt__c.KeyPrefix}/e?retURL={!Cash_Receipt__c.Id}" value="New {!$ObjectType.AcctSeed__Cash_Receipt__c.label}" rendered="{!isSaved}" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection id="cashReceiptInfo" title="Cash Receipt" columns="1" collapsible="false" rendered="{!billCashWrapperList.size>0 && AcctSeed__Cash_Receipt__c.AcctSeed__Status__c='Posted' && !isLastModifiedError}">
                <apex:dataTable value="{!receipts}" var="receipt" id="theTable" width="100%">
                    <apex:column value="{!receipt.AcctSeed__Account__c}">
                        <apex:facet name="header">Customer Name</apex:facet>
                    </apex:column>
                    <apex:column value="{!receipt.AcctSeed__Purpose__c}">
                        <apex:facet name="header">{!$ObjectType.AcctSeed__Cash_Receipt__c.Fields.AcctSeed__Purpose__c.Label}</apex:facet>
                    </apex:column>
                    <apex:column value="{!receipt.AcctSeed__Payment_Reference__c}">
                        <apex:facet name="header">{!$ObjectType.AcctSeed__Cash_Receipt__c.Fields.AcctSeed__Payment_Reference__c.Label}</apex:facet>
                    </apex:column>
                    <apex:column id="receiptDate" value="{!receipt.AcctSeed__Receipt_Date__c}">
                        <apex:facet name="header">{!$ObjectType.AcctSeed__Cash_Receipt__c.Fields.AcctSeed__Receipt_Date__c.Label}</apex:facet>
                    </apex:column>
                    <apex:column id="applyAmount" value="{!receipt.AcctSeed__Amount__c}">
                        <apex:facet name="header">{!$ObjectType.AcctSeed__Cash_Receipt__c.Fields.AcctSeed__Amount__c.Label}</apex:facet>
                    </apex:column>
                    <apex:column >
                        <apex:outputText id="applyAppliedAmount"/>
                        <apex:facet name="header">{!$ObjectType.AcctSeed__Cash_Receipt__c.Fields.AcctSeed__Applied_Amount__c.Label}</apex:facet>
                    </apex:column>
                    <apex:column style="width:180px">
                        <apex:outputText id="applyBalanceAmount" value="{0, number, $#,##0.00;($#,##0.00)}">
                            <apex:param value="{!CashReceiptBalance}" />
                        </apex:outputText>
                        <apex:facet name="header">Cash Receipt Balance</apex:facet>
                    </apex:column>
                </apex:dataTable>
            </apex:pageBlockSection>
            <apex:PageBlockSection id="theBillingSection" title="Billings" columns="1" collapsible="false" rendered="{!billCashWrapperList.size>0 && AcctSeed__Cash_Receipt__c.AcctSeed__Status__c='Posted' && !isLastModifiedError}">
                <apex:pageBlockTable id="theBillingTable" value="{!billCashWrapperList}" var="item" >                   
                    <apex:column style="vertical-align:top">
                        <apex:facet name="header">Billing Name</apex:facet>
                        <apex:outputField value="{!item.billCash.AcctSeed__Billing__c}" />
                    </apex:column>
                    <apex:column value="{!item.bill.AcctSeed__Proprietary_Billing_Number__c}" style="vertical-align:top" />
                    <apex:column value="{!item.bill.AcctSeed__Date__c}" style="vertical-align:top" />
                    <apex:column style="vertical-align:top">
                        <apex:facet name="header">Billing Terms</apex:facet>
                        <apex:outputField value="{!item.bill.AcctSeed__Billing_Terms_Name__c}" />
                    </apex:column>
                    <apex:column style="vertical-align:top;text-align:right" value="{!item.bill.AcctSeed__Discount_Amount__c}" headerClass="alignRight" />
                    <apex:column id="discountDate" value="{!item.bill.AcctSeed__Discount_Due_Date__c}" style="vertical-align:top" />
                    <apex:column value="{!item.bill.AcctSeed__Due_Date__c}" style="vertical-align:top" />
                    <apex:column style="vertical-align:top;text-align:right" footerClass="alignRight" headerClass="alignRight">
                        <apex:facet name="header">Billing Amount</apex:facet>
                        <apex:outputField id="billingAmount" value="{!item.bill.AcctSeed__Total__c}"/>
                        <apex:facet name="footer">
                            <apex:outputText id="billingTotalAmount"/>
                        </apex:facet>
                    </apex:column>
                    <apex:column style="width:260px;vertical-align:top;text-align:right" footerClass="alignRight" headerClass="alignRight">
                        <apex:facet name="header">Total Applied</apex:facet>
                        <apex:outputText id="appliedAmountOrig" value="{!item.AppliedAmount - item.billCash.AcctSeed__Applied_Amount__c}" style="display:none"/>
                        <apex:outputText id="appliedAmount" value="{0, number, $#,##0.00;($#,##0.00)}">
                            <apex:param value="{!item.AppliedAmount}" />
                        </apex:outputText>
                        <apex:facet name="footer">
                            <apex:outputText id="appliedTotalAmount"/>
                        </apex:facet>
                    </apex:column>
                    <apex:column id="theBillingBalance" style="width:260px;vertical-align:top;text-align:right" footerClass="alignRight" headerClass="alignRight">
                        <apex:facet name="header">Billing Balance</apex:facet>
                        <apex:outputText id="balanceAmount" value="{0, number, $#,##0.00;($#,##0.00)}" >
                            <apex:param value="{!item.Balance}" />
                        </apex:outputText>
                        <apex:facet name="footer">
                            <apex:outputText id="balanceTotalAmount"/>
                        </apex:facet>
                    </apex:column>
                    <apex:column style="width:80px;text-align:center;vertical-align:top;" rendered="{!!isSaved}">
                        <apex:outputPanel id="buttonPanel" layout="inline">
                            <apex:commandLink styleClass="buttonLink" id="from" onclick="javascript:AcctSeed.ASModule.populateReceivedAmount('{!item.lineNum}');return false;"  style="display:{!IF(!ISBLANK(item.billCash.AcctSeed__Applied_Amount__c) && !item.isPeriodClosed,'none','')}">
                                <button type="button" class="arrowButton" >&#x2192;</button>
                            </apex:commandLink>
                            <apex:commandLink styleClass="buttonLink" id="to" onclick="javascript:AcctSeed.ASModule.clearReceivedAmount('{!item.lineNum}');return false;" style="display:{!IF(!!ISBLANK(item.billCash.AcctSeed__Applied_Amount__c) && !item.isPeriodClosed,'none','')}">
                                <button type="button" class="arrowButton">&#x2190;</button>
                            </apex:commandLink>
                        </apex:outputPanel>
                    </apex:column>
                    <apex:column style="width:150px;vertical-align:top;text-align:right" footerClass="alignRight" headerClass="alignRight">
                        <apex:facet name="header">Received Amount</apex:facet>
                        <apex:inputField id="receivedAmount" style="width:80px;text-align:right" value="{!item.billCash.AcctSeed__Applied_Amount__c}" onkeyup="AcctSeed.ASModule.updateReceivedAmount('{!item.lineNum}',this)" onchange="AcctSeed.ASModule.updateReceivedAmount('{!item.lineNum}',this)" required="false" rendered="{!!isSaved && !item.isPeriodClosed}">
                        </apex:inputField>
                        <apex:outputField value="{!item.billCash.AcctSeed__Applied_Amount__c}" rendered="{!isSaved || item.isPeriodClosed}" />
                        <apex:facet name="footer">
                            <apex:outputText id="receivedTotalAmount"/>
                        </apex:facet>
                    </apex:column>
                    <apex:column style="width:150px;vertical-align:top;">
                        <apex:facet name="header">Accounting Period</apex:facet>
                        <apex:outputPanel styleClass="requiredInput" style="margin-left:5px" layout="block">
                            <apex:outputPanel id="accountingPeriodPanel" layout="block">
                                <apex:outputPanel id="accountingPeriodRequired" styleClass="requiredBlock" style="display:{!IF(ISBLANK(item.billCash.AcctSeed__Applied_Amount__c),'none','')}" />
                            </apex:outputPanel>
                            <apex:inputField style="width:60px" value="{!item.billCash.AcctSeed__Accounting_Period__c}" rendered="{!!isSaved && !item.isPeriodClosed}" required="false" />
                        </apex:outputPanel>
                        <apex:outputField style="width:60px" value="{!item.billCash.AcctSeed__Accounting_Period__c}" rendered="{!isSaved || item.isPeriodClosed}" />
                    </apex:column>
                    <apex:column style="width:100px;vertical-align:top;text-align:right" footerClass="alignRight" headerClass="alignRight">
                        <apex:facet name="header">Adjustment Amount</apex:facet>
                        <apex:outputField style="text-align:right;width:80px" value="{!item.billCash.AcctSeed__Adjustment_Amount__c}" rendered="{!isSaved || item.isPeriodClosed}" />
                        <apex:inputField id="adjustmentAmount" style="text-align:right;width:80px" value="{!item.billCash.AcctSeed__Adjustment_Amount__c}" onkeyup="AcctSeed.ASModule.updateAdjustmentAmount('{!item.lineNum}',this)" onchange="AcctSeed.ASModule.updateAdjustmentAmount('{!item.lineNum}',this)" rendered="{!NOT(isSaved) && NOT(item.isPeriodClosed)}"/>
                        <apex:facet name="footer">
                            <apex:outputText id="adjustmentTotalAmount"/>
                        </apex:facet>
                    </apex:column>
                    <apex:column style="vertical-align:top">
                        <apex:facet name="header">Adjustment GL Account</apex:facet>
                        <apex:outputField value="{!item.billCash.AcctSeed__Adjustment_GL_Account__c}" rendered="{!isSaved || item.isPeriodClosed}" />
                        <apex:outputPanel id="adjustmentGLAccountPanel" styleClass="requiredInput" layout="block" rendered="{!NOT(isSaved) && NOT(item.isPeriodClosed)}">
                            <apex:outputPanel id="adjustmentGLAccountRequired" styleClass="requiredBlock" layout="block" style="display:{!IF(ISBLANK(item.billCash.AcctSeed__Adjustment_Amount__c),'none','')}" />
                            <apex:inputField value="{!item.billCash.AcctSeed__Adjustment_GL_Account__c}" />
                        </apex:outputPanel>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>