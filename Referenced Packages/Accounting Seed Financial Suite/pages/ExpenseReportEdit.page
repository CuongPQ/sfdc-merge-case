<apex:page standardController="AcctSeed__Expense_Report__c" id="thePage" extensions="AcctSeed.ExpenseReportControllerExt" sidebar="false">
    <apex:stylesheet value="/sCSS/21.0/sprites/1297816277000/Theme3/default/gc/versioning.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">
        var j$ = jQuery.noConflict();

        function disableButtons(label) {
            j$('.btn').each(function(elem) {
                this.className = 'btnDisabled';
                this.disabled = true;
                this.value = label;
            })
        }

        j$(document).ready(function() {
            j$("body").keydown(function(e) {
                if (e.keyCode == 13 && !j$(":focus").is("textarea")) {
                    saveAndClose();
                }
            });
        });
    </script>
    <style type="text/css">
        .action-col {
            width: 45px;
            font-size: 10px;
        }
        
        .project-col {
            width: 160px;
        }
        
        .project-task-col {
            width: 170px;
        }
        
        .bill-reim-col {
            width: 60px
        }
        
        .date-col {
            width: 150px
        }
        
        .mileage-col {
            width: 140px
        }
        
        .miles-col {
            width: 60px
        }
        
        .amt-col {
            width: 80px
        }
        
        .com-col {
            width: 160px;
        }
        
        .align-right {
            text-align: right;
        }
        
        .footer-row {
            font-weight: bold;
            background-color: #e3e3d7;
        }
        
        .apexp .totalRow {
            background-color: #e3e3d7;
            font-weight: bold;
            padding-bottom: 4px;
            padding-top: 4px;
        }
        .btn.refreshButton {
            height: 18px;
            background: url(/img/alohaSkin/sync.png) top left no-repeat;
            width: 22px;
            vertical-align: bottom;
            background-position: 2px 1px;
        } 
        .btn.refreshButton:hover {
            background-position: 2px 1px;
        }
    </style>
    <apex:form id="theForm">
        <apex:sectionHeader title="Expense Report" subtitle="{!AcctSeed__Expense_Report__c.Name}" />
        <apex:pageBlock id="thePageBlockHeader" mode="maindetail" title="Expense Report Edit">
            <apex:pageMessages />
            <apex:pageBlockButtons location="top">
                <apex:actionStatus id="saveStatus" onstart="disableButtons('Saving...')" />
                <apex:commandButton status="saveStatus" value="Save & Refresh" reRender="theForm" action="{!saveAndRefresh}" />
                <apex:commandButton status="saveStatus" value="Save & Complete" reRender="theForm" action="{!saveAndClose}" />
                <apex:commandButton value="Cancel" immediate="true" action="{!cancel}" />
                <apex:actionFunction name="saveAndClose" action="{!saveAndClose}" status="saveStatus" reRender="theForm" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection id="reportData" columns="2">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Name" />
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel styleClass="requiredBlock" />
                        <apex:inputField value="{!expReport.Name}" required="false" />
                        <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!isNameError}">
                            <strong>Error:</strong> You must enter a value
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Employee" />
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel styleClass="requiredBlock" rendered="{!$ObjectType.AcctSeed__Expense_Report__c.Fields.AcctSeed__Status__c.Updateable}" />
                        <apex:inputField value="{!expReport.AcctSeed__Employee__c}" />
                        <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!isEmployeeError}">
                            <strong>Error:</strong> You must enter a value
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Total Expenses" />
                    <apex:outputText id="expenseAmountTotal" value="{0, number, $###,##0.00}">
                        <apex:param value="{!expenseAmountTotal}" />
                    </apex:outputText>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Status" />
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel styleClass="requiredBlock" rendered="{!$ObjectType.AcctSeed__Expense_Report__c.Fields.AcctSeed__Status__c.Updateable}" />
                        <apex:inputField value="{!expReport.AcctSeed__Status__c}" />
                        <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!isStatusError}">
                            <strong>Error:</strong> You must enter a value
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <br/>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <apex:pageBlock id="thePageBlock" title="Expense Report Edit">
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton status="saveStatus" value="Save & Refresh" reRender="theForm" action="{!saveAndRefresh}" />
                <apex:commandButton status="saveStatus" value="Save & Complete" reRender="theForm" action="{!saveAndClose}" />
                <apex:commandButton value="Cancel" immediate="true" action="{!cancel}" />
            </apex:pageBlockButtons>
            <!-- Create Tab panel -->
            <apex:tabPanel switchType="server" selectedTab="tab1" id="ExpenseTabPanel" styleClass="theTabPanel" tabClass="theTabPanel" contentClass="tabContent" activeTabClass="activeTab" inactiveTabClass="inactiveTab">
                <apex:tab label="Expenses" name="tab1" id="tabOne" styleClass="">
                    <apex:outputPanel id="tableSection1">
                        <apex:pageBlockTable value="{!expensesList}" var="item" id="billableExpenses" footerClass="footer-row">
                            <apex:column headerClass="action-col">
                                <apex:actionStatus id="mydeleteStatus1">
                                    <apex:facet name="stop">
                                        <apex:actionRegion >
                                            <apex:commandLink action="{!deleteExpenseLine}" title="Remove line" status="mydeleteStatus1" rerender="theForm">
                                                <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/red-x.png')}" alt="Remove line" />
                                                <apex:param id="rowCount1" name="rowCount1" assignTo="{!selectedExpenseRow}" value="{!item.intRowNum}" />
                                            </apex:commandLink>
                                        </apex:actionRegion>
                                    </apex:facet>
                                    <apex:facet name="start">
                                        <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/red-x.png')}" />
                                    </apex:facet>
                                </apex:actionStatus>
                                <apex:facet name="footer">
                                    <apex:commandlink rerender="tableSection1" title="Add line" action="{!addExpenseLine}">
                                        <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/green-cross.png')}" alt="Add line" /><span style="position:absolute;margin-left:1px;margin-top:2px;text-decoration:underline">Line</span>
                                    </apex:commandlink>
                                </apex:facet>
                            </apex:column>
                            <apex:column headerValue="Project" headerClass="project-col">
                                <apex:actionRegion >
                                    <apex:outputPanel layout="block" styleClass="requiredInput">
                                        <apex:outputPanel styleClass="requiredBlock" />
                                        <apex:inputField value="{!item.expenseLine.AcctSeed__Project__c}">
                                            <apex:actionSupport event="onchange" rerender="projTaskId" status="reloadStatus">
                                                <apex:param id="selectedRow1" name="selectedRow1" assignTo="{!selectedExpenseRow}" value="{!item.intRowNum}" />
                                            </apex:actionSupport>
                                        </apex:inputField>
                                        <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isProjectError}">
                                            <strong>Error:</strong> You must enter a value
                                        </apex:outputPanel>
                                    </apex:outputPanel>
                                </apex:actionRegion>
                            </apex:column>
                            <apex:column headerValue="Project Task" headerClass="project-task-col">
                                <apex:actionStatus id="reloadStatus">
                                    <apex:facet name="start">
                                        <apex:image value="/img/loading.gif" title="Processing..." />
                                    </apex:facet>
                                    <apex:facet name="stop">
                                        <apex:outputPanel id="projTaskId" style="white-space: nowrap" layout="block" styleClass="requiredInput">
                                            <apex:outputPanel styleClass="requiredBlock" />
                                            <apex:selectList value="{!item.expenseLine.AcctSeed__Project_Task__c}" size="1" title="Project Tasks">
                                                <apex:selectOptions value="{!item.ProjectTasks}" id="tasks" />
                                            </apex:selectList>&nbsp;
                                            <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isProjectTaskError}">
                                                <strong>Error:</strong> You must enter a value
                                            </apex:outputPanel>
	                                        <apex:commandButton styleClass="refreshButton" rerender="projTaskId" status="reloadStatus" title="Refresh Project Task">
	                                            <apex:param id="selectedRow1" name="selectedRow1" assignTo="{!selectedExpenseRow}" value="{!item.intRowNum}" />
	                                        </apex:commandButton>                                              
                                        </apex:outputPanel>                                      
                                    </apex:facet>
                                </apex:actionStatus>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Billable__c.Label}" headerClass="bill-reim-col" rendered="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Billable__c.Accessible}">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Billable__c}" />
                            </apex:column>
                            <apex:column headerClass="bill-reim-col" rendered="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Employee_Reimburse__c.Accessible}">
                                <apex:facet name="header">
                                    <apex:outputText >Employee
                                        <br/>Reimburse</apex:outputText>
                                </apex:facet>
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Employee_Reimburse__c}" />
                            </apex:column>
                            <apex:column headerValue="Credit Card Vendor" headerClass="project-task-col" rendered="{!IF(ccVendorList.size>1,'','none')}">
                                <apex:selectList value="{!item.ccVendorId}" size="1" style="width:100px">
                                    <apex:selectOptions value="{!ccVendorList}" />
                                </apex:selectList>
                                <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isReimburseAndCCVendorError}">
                                    <strong>Error:</strong> The employee reimburse field and the credit card vendor field cannot both be populated. You can only select one.
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="Date" headerClass="date-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:inputField value="{!item.expenseLine.AcctSeed__Date__c}" required="false" />
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isDateError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="Expense Type" headerClass="project-task-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:selectList value="{!item.strExpenseTypeId}" size="1" title="Type">
                                        <apex:selectOptions value="{!expenseTypesList}" />
                                    </apex:selectList>
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isExpenseTypeError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Amount__c.Label}" headerClass="amt-col" footerClass="align-right">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:actionRegion >
                                        <apex:inputField value="{!item.expenseLine.AcctSeed__Amount__c}" style="width:80px;text-align:right">
                                            <apex:actionSupport event="onkeyup" rerender="projectTotal,expenseAmountTotal" />
                                            <apex:actionSupport event="onblur" rerender="projectTotal,expenseAmountTotal" />
                                        </apex:inputField>
                                    </apex:actionRegion>
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isAmountError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                                <apex:facet name="footer">
                                    <apex:outputPanel id="projectTotal">
                                        <apex:outputText value="{0, number, $###,##0.00}" rendered="{!expensesList.size>0}">
                                            <apex:param value="{!projectAmountTotal}" />
                                        </apex:outputText>
                                    </apex:outputPanel>
                                </apex:facet>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Internal_Comment__c.Label}" headerClass="com-col">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Internal_Comment__c}" />
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Invoice_Comment__c.Label}">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Invoice_Comment__c}" />
                            </apex:column>
                        </apex:pageBlockTable>
                    </apex:outputPanel>
                </apex:tab>
                <apex:tab label="Mileage" id="tabTwo">
                    <apex:outputPanel id="tableSection2">
                        <apex:pageBlockTable value="{!mileageList}" var="item" id="mileageExpenses" width="100%" styleClass="column">
                            <apex:column headerClass="action-col">
                                <apex:actionStatus id="mydeleteStatus2">
                                    <apex:facet name="stop">
                                        <apex:actionRegion >
                                            <apex:commandLink action="{!deleteMileageLine}" title="Remove line" status="mydeleteStatus2" rerender="theForm" immediate="true">
                                                <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/red-x.png')}" alt="Remove line" />
                                                <apex:param id="rowCount2" name="rowCount2" assignTo="{!selectedMileageRow}" value="{!item.intRowNum}" />
                                            </apex:commandLink>
                                        </apex:actionRegion>
                                    </apex:facet>
                                    <apex:facet name="start">
                                        <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/red-x.png')}" />
                                    </apex:facet>
                                </apex:actionStatus>
                                <apex:facet name="footer">
                                    <apex:commandlink rerender="tableSection2" title="Add line" action="{!addMileageLine}">
                                        <apex:image url="{!URLFOR($Resource.AcctSeed__accounting_resources, 'images/green-cross.png')}" alt="Add line" /><span style="position:absolute;margin-left:1px;margin-top:2px;text-decoration:underline">Line</span>
                                    </apex:commandlink>
                                </apex:facet>
                            </apex:column>
                            <apex:column headerValue="Project" headerClass="project-col">
                                <apex:actionRegion >
                                    <apex:outputPanel layout="block" styleClass="requiredInput">
                                        <apex:outputPanel styleClass="requiredBlock" />
                                        <apex:inputField value="{!item.expenseLine.AcctSeed__Project__c}" style="width:120px">
                                            <apex:actionSupport event="onchange" rerender="projTaskId" status="reloadStatus">
                                                <apex:param id="selectedRow2" name="selectedRow2" assignTo="{!selectedMileageRow}" value="{!item.intRowNum}" />
                                            </apex:actionSupport>
                                        </apex:inputField>
                                        <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isProjectError}">
                                            <strong>Error:</strong> You must enter a value
                                        </apex:outputPanel>
                                    </apex:outputPanel>
                                </apex:actionRegion>
                            </apex:column>
                            <apex:column headerValue="Project Task" headerClass="project-task-col">
                                <apex:actionStatus id="reloadStatus">
                                    <apex:facet name="start">
                                        <apex:image value="/img/loading.gif" title="Processing..." />
                                    </apex:facet>
                                    <apex:facet name="stop">
                                        <apex:outputPanel id="projTaskId" layout="block" styleClass="requiredInput">
                                            <apex:outputPanel styleClass="requiredBlock" />
                                            <apex:selectList value="{!item.expenseLine.AcctSeed__Project_Task__c}" size="1" title="task">
                                                <apex:selectOptions value="{!item.ProjectTasks}" id="tasks" />
                                            </apex:selectList>&nbsp;
                                            <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isProjectTaskError}">
                                                <strong>Error:</strong> You must enter a value
                                            </apex:outputPanel>
	                                        <apex:commandButton styleClass="refreshButton" rerender="projTaskId" status="reloadStatus" title="Refresh Project Task">
	                                            <apex:param id="selectedRow2" name="selectedRow2" assignTo="{!selectedMileageRow}" value="{!item.intRowNum}" />
	                                        </apex:commandButton>                                             
                                        </apex:outputPanel>                                       
                                    </apex:facet>
                                </apex:actionStatus>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Billable__c.Label}" rendered="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Billable__c.Accessible}" headerClass="bill-reim-col">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Billable__c}" />
                            </apex:column>
                            <apex:column rendered="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Employee_Reimburse__c.Accessible}" headerClass="bill-reim-col">
                                <apex:facet name="header">
                                    <apex:outputText >Employee
                                        <br/>Reimburse</apex:outputText>
                                </apex:facet>
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Employee_Reimburse__c}" />
                            </apex:column>
                            <apex:column headerValue="Date" headerClass="date-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:inputField value="{!item.expenseLine.AcctSeed__Date__c}" required="false" />
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isDateError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="Expense Type" headerClass="project-task-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:actionRegion >
                                        <apex:selectList value="{!item.strExpenseTypeId}" size="1" title="Type">
                                            <apex:selectOptions value="{!mileageTypesList}" />
                                            <apex:actionSupport event="onchange" rerender="mileageAmount,mileageMilesTotal,mileageAmountTotal,expenseAmountTotal" />
                                        </apex:selectList>
                                    </apex:actionRegion>
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isExpenseTypeError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Mileage_Origin__c.Label}" headerClass="mileage-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:inputField value="{!item.expenseLine.AcctSeed__Mileage_Origin__c}" />
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isOriginError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Mileage_Destination__c.Label}" headerClass="mileage-col">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:inputField value="{!item.expenseLine.AcctSeed__Mileage_Destination__c}" />
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isDestinationError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Miles__c.Label}" headerClass="miles-col" footerClass="align-right">
                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                    <apex:outputPanel styleClass="requiredBlock" />
                                    <apex:actionRegion >
                                        <apex:inputField value="{!item.expenseLine.AcctSeed__Miles__c}" style="width:50px;text-align:right">
                                            <apex:actionSupport event="onkeyup" rerender="mileageAmount,mileageMilesTotal,mileageAmountTotal,expenseAmountTotal" />
                                            <apex:actionSupport event="onblur" rerender="mileageAmount,mileageMilesTotal,mileageAmountTotal,expenseAmountTotal" />
                                        </apex:inputField>
                                    </apex:actionRegion>
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isMilesError}">
                                        <strong>Error:</strong> You must enter a value
                                    </apex:outputPanel>
                                    <apex:outputPanel styleClass="errorMsg" layout="block" rendered="{!item.isMilesNegativeError}">
                                        <strong>Error:</strong> Cannot be a negative number
                                    </apex:outputPanel>
                                </apex:outputPanel>
                                <apex:facet name="footer">
                                    <apex:outputPanel id="mileageMilesTotal">
                                        <apex:outputText value="{!MileageMilesTotal}" rendered="{!mileageList.size>0}" />
                                    </apex:outputPanel>
                                </apex:facet>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Amount__c.Label}" styleClass="align-right" headerClass="amt-col" footerClass="align-right">
                                <apex:outputText id="mileageAmount" value="{0, number, $###,##0.00}">
                                    <apex:param value="{!item.amount}" />
                                </apex:outputText>
                                <apex:facet name="footer">
                                    <apex:outputPanel id="mileageAmountTotal">
                                        <apex:outputText value="{0, number, $###,##0.00}" rendered="{!mileageList.size>0}">
                                            <apex:param value="{!MileageAmountTotal}" />
                                        </apex:outputText>
                                    </apex:outputPanel>
                                </apex:facet>
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Internal_Comment__c.Label}" headerClass="com-col">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Internal_Comment__c}" />
                            </apex:column>
                            <apex:column headerValue="{!$ObjectType.AcctSeed__Expense_Line__c.fields.AcctSeed__Invoice_Comment__c.Label}">
                                <apex:inputField value="{!item.expenseLine.AcctSeed__Invoice_Comment__c}" />
                            </apex:column>
                        </apex:pageBlockTable>
                    </apex:outputPanel>
                </apex:tab>
            </apex:tabPanel>
        </apex:pageBlock>
    </apex:form>
</apex:page>