<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Financial Suite app for booking transactions, reconciling and reporting on the general ledger.</description>
    <label>General Ledger</label>
    <tab>Accounting_Period__c</tab>
    <tab>Journal_Entry__c</tab>
    <tab>Scheduled_Revenue_Expense__c</tab>
    <tab>Run_Financial_Report</tab>
    <tab>Financial_Report_Definition__c</tab>
    <tab>Bank_Deposit__c</tab>
    <tab>Bank_Reconciliation2__c</tab>
    <tab>Accounting_Help</tab>
</CustomApplication>
