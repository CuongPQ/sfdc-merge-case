@isTest
public class AMC_MergeCase_Test {
    
    @testSetup static void methodName() {
    
        List<Case> listCase = new List<Case>();
        
        Id generalRecordTypeId;
        
        if (Schema.SObjectType.Case.getRecordTypeInfosByName() != null && 
                Schema.SObjectType.Case.getRecordTypeInfosByName().get('General_Case') != null) {
            
            generalRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Case').getRecordTypeId();
        }
        
        List<AMC_Case_Merge_Settings__c> listFieldSetting = new List<AMC_Case_Merge_Settings__c>();
        
        AMC_Case_Merge_Settings__c item1 = new AMC_Case_Merge_Settings__c(Name='Case Origin', APIName__c='Origin');
        
        AMC_Case_Merge_Settings__c item2 = new AMC_Case_Merge_Settings__c(Name='Case Status', APIName__c='Status');
        
        AMC_Case_Merge_Settings__c item3 = new AMC_Case_Merge_Settings__c(Name='Subject', APIName__c='Subject');
        
        AMC_Case_Merge_Settings__c item4 = new AMC_Case_Merge_Settings__c(Name='Created Date', APIName__c='CreatedDate');
        
        listFieldSetting.add(item1);
        
        listFieldSetting.add(item2);
        
        listFieldSetting.add(item3);
        
        listFieldSetting.add(item4);
        
        insert listFieldSetting;
        //################################## 
        
        for (Integer i = 0; i < 10; i++) {
        
            Case item = new Case();
            
            item.Subject = 'Test_Merge_Case_' + i;
            
            if (generalRecordTypeId != null) {
            
                item.RecordTypeId = generalRecordTypeId;
            }
            
            //item.VisualAntidote__Fast_Forms_Auto_Assign__c = true;
            
            //item.VisualAntidote__Fast_Forms_Autoresponse__c = true;
            
            if (Math.mod(i,5) == 0 ) {
            
                item.Origin='Phone';
                item.Status = 'New';
            } 
            else if (Math.mod(i,5) == 1) {
            
                item.Origin='Email';
                item.Status = 'On Hold';
            }
            else if (Math.mod(i,5) == 2) {
            
                item.Origin='Web';
                item.Status = 'New';
            }
            else if (Math.mod(i,5) == 3) {
            
                item.Origin='New';//'Order Email';
                item.Status = 'On Hold';
            }
            else if (Math.mod(i,5) == 4) {
            
                item.Origin='New';//'Billing Email';
                item.Status = 'New';
            }
            
            listCase.add(item);
        }
        
        insert(listCase);
        
        System.debug('[AMC_MergeCase_Test] - testSetup - listcase: ' + listCase);
        
        //###########################################
        
        List<Task> listTask = new List<Task>();
        
        for (Integer i=0; i < 10; i++) {
            
            Task item = new Task();
            
            item.Status = 'Open';
            
            item.ActivityDate = System.Today().adddays(10);
            
            if (Math.mod(i,4) == 0 ) {
            
                item.Subject='Call';
            } 
            else if (Math.mod(i,4) == 1) {
            
                item.Subject='Send Letter';
            }
            else if (Math.mod(i,4) == 2) {
            
                item.Subject='Send Quote';
            }
            else if (Math.mod(i,4) == 3) {
            
                item.Subject='Other';
            }
            
            listTask.add(item);
        }
        
        insert listTask;
        
        System.debug('[AMC_MergeCase_Test] - testSetup - listTask: ' + listTask);
        
        //###########################################
        
        List<CaseComment> listCaseCmt = new List<CaseComment>();
        
        for (Integer i= 0; i < 10; i++) {
        
            CaseComment item = new CaseComment(CommentBody='cmt_test_' +i,
            ParentId= listCase.get(i).Id);
            
            listCaseCmt.add(item);
        }
        
        insert listCaseCmt;
        
        System.debug('[AMC_MergeCase_Test] - testSetup - listCaseCmt: ' + listCaseCmt);
        
        //###########################################
        
        List<EmailMessage> listEmail = new List<EmailMessage>();
        
        for (Integer i= 0; i < 10; i++) {
        
            EmailMessage item = new EmailMessage(ActivityId = listTask.get(i).Id,
                BccAddress = 'test_bcc@vn.com.vn', CcAddress='test_cc@vn.com.vn', 
                FromAddress='test_from@vn.com.vn', FromName='Test_' + i, 
                HtmlBody='Test_body_' + i, MessageDate=System.today(), 
                ParentId=listCase.get(i).Id, Subject='Sb_Test_' + i,
                ToAddress='demo@vnn.com.vn');
            
            listEmail.add(item);
        }
        
        insert listEmail;
        
        System.debug('[AMC_MergeCase_Test] - testSetup - listEmail: ' + listEmail);
        
        //###########################################
        List<FeedItem> litsFeed = new List<FeedItem>();
        
        for (Integer i= 0; i < 10; i++) {
        
            FeedItem item = new FeedItem(Body= 'test_body_' + i, 
                ParentId=listCase.get(i).Id, Title='Title_Test_' + i,
                Type='TextPost');
            
            litsFeed .add(item);
        }
        
        insert litsFeed ;
        
        System.debug('[AMC_MergeCase_Test] - testSetup - litsFeed : ' + litsFeed );
        
        //###########################################
    }
    
    public static testmethod void initTest() {
    
        String listCaseId ='';
        
        List<Case> listCa = [SELECT id FROM Case limit 10];
        
        for (Case item: listCa) {
        
            listCaseId += ',' + item.id;
        }
        
        if (listCaseId.length() > 0 ) {
        
            listCaseId.replaceFirst(',','');
        }
    
        PageReference tpageRef = Page.AMC_Merge_Case_Page;
        
        Test.setCurrentPage(tpageRef);

        ApexPages.currentPage().getParameters().put('list_case_id', listCaseId);
        
        AMC_MergeCaseCtrl con = new AMC_MergeCaseCtrl();
        
        System.assert(con.actionCancel() != null);
        
        con.finalCaseId = listCa[0].id;
        
        con.finalCase = listCa[5].clone(false,true,false,false);

        System.assert(con.actionMerge() != null);

    }
}