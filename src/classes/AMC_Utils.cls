/**
* @Author: CuongPQ
* @Date: 2016 Aug 04. 
*/
global class AMC_Utils
{
    global class AMC_Utils_Exception extends Exception {}

    global static Map <String, Schema.SObjectField> getObjectDescribe(String objName)
    {
        try
        {
            return Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        }
        catch (Exception e)
        {
            throw new AMC_Utils_Exception('[AMC_Utils]-(getObjectDescribe)- ERROR', e);
        }
    }
    
    global static String getAllObjectApiFieldInString(String objName)
    {
        try
        {
            String allSet = '';
            for(Schema.SObjectField sfield : getObjectDescribe(objName).Values())
            {
                schema.describefieldresult dfield = sfield.getDescribe();
                allSet += ', ' + dfield.getname();
            }
            return allSet.replaceFirst(', ', '');
        }
        catch(Exception e)
        {
            throw new AMC_Utils_Exception('[AMC_Utils]-(getObjectDescribe)- ERROR', e);
        }
    }
    
    global static String getRecordTypeIdByDevNameAndObject(String recordType, String objName)
    {
        Schema.DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objName).getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = sobjectResult.getRecordTypeInfos();
        Map<String,Id> mapofCaseRecordTypeNameandId = new Map<String,Id>();
        for(Schema.RecordTypeInfo info : recordTypeInfo){
            if (info.getName() == recordType)
                return info.getRecordTypeId();                
        }
        return recordTypeInfo.size() > 0 ? recordTypeInfo[0].getRecordTypeId() :null;
    }
    
    /**
    * --getValidateJobRan--
    * @author: CuongPQ.
    * @created date: 2016 Aug 06
    * @desciption: check this business is running by another job or not.
    * @return: Boolean
    *    - true: have no job run to calculate.
    *    - false: have at least once job is running/holding/queue/pending.
    *
    */
    public static boolean getValidateJobRan(String apxClsName){
        
        return [SELECT Status FROM AsyncApexJob 
                WHERE apexclass.Name =:apxClsName AND 
                Status NOT IN ('Aborted','Completed', 'Failed') LIMIT 1].size() == 0;
    }
    
    /*
    global static Id getProfileIdByName(string pName)
    {
        List<Profile> result = [SELECT Id, Name FROM Profile WHERE name=:pName];
        return result.size() > 0 ? result[0].Id : null;
    }
    */
}