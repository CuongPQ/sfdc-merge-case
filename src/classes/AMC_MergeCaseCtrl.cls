/**
* @Authod: CuongPQ.
* @created date: 2016 Sep 05.
* @description: handle merge case from list case id.
*/
public with sharing  class AMC_MergeCaseCtrl {

    //define related name object should be merge.
    /*
    private Static Final Map<String, String> mapObjRelated = new Map<String, String> {
        'CaseComment' => 'ParentId',
        'EmailMessage' => 'ParentId',
        'FeedComment' => 'ParentId',
        'FeedItem' => 'ParentId',
        'OpenActivity' => 'OpenActivity', 
        'ActivityHistory' => 'whatId'}; 
    */
    
    //define constants select all field name.
    public Static Final String AMC_CASE_NUMBER = 'CaseNumber';
    
    //define constants Created Date field name.
    public Static Final String AMC_CREATE_DATE = 'CreatedDate';
    
    //define constants Created By field name.
    public Static Final String AMC_CREATE_BY = 'CreatedById';
    
    //define constants last modify date field name.
    public Static Final String AMC_LAST_MOFIFIED_DATE = 'LastmodifiedDate';
    
    //define constants last modify by field name.
    public Static Final String AMC_LAST_MOFIFIED_BY = 'LastmodifiedById';
    
    //define constants case id.
    public Static Final String AMC_CASE_ID = 'Id';

    //Store the list case object.
    public List<Case> listCase {get;set;}
    
    //store the final case merged.
    public Case finalCase {get;set;}
    
    //storage final case id from merge form.
    public String finalCaseId {get;set;}
    
    //storage list fields render.
    public Map<String, String> mapFields {get;set;}
    
    //list storage index have field difference.
    public Map<String, Boolean> mapRowCaseDiff {get;set;}
    
    //store the list case id.
    public String strListId {get;set;}
    
    public AMC_MergeCaseCtrl() {
    
        initData();
    }
    
    public void initData() {
    
        try {
        
            this.strListId = ApexPages.currentPage().getParameters().get('list_case_id');
            
            System.debug('[AMC_MergeCaseCtrl] - contructor - the list case id parameter: ' + this.strListId);
            
            if (String.isBlank(this.strListId)) {
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Can not find any Cases'));
            }
            
            else {
                
                this.mapFields = parseMapFieldsData();
                
                String[]pListId = this.strListId.split(',');
                
                this.listCase = Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('Case') 
                                                + ' FROM Case WHERE Id IN :pListId ORDER BY CreatedDate ASC');
                
                this.strListId = '';
                
                for (Case item : this.listCase) {
                
                    this.strListId += ',' + item.Id;
                }
                
                if (String.isNotBlank(this.strListId)) {
                
                    this.strListId = this.strListId.replaceFirst(',', '');
                }
                                            
                this.mapRowCaseDiff = buildListCaseDifference(this.listCase, this.mapFields.keySet());
            }
            
            this.finalCase = new Case();
            
            System.debug('[AMC_MergeCaseCtrl] - initData - the list case: ' + this.listCase);
            
            if (this.listCase.size() < 2 || this.listCase.size() > 10) {
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.AMC_MSG_ERROR_LIST_MERGE_CASE_RANGE));
            }
        }
        catch(Exception e) {
        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            
            System.debug('[AMC_MergeCaseCtrl] - initData - error : ' + e.getmessage());
        }
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 06
    * @desciption: read custom settings list and parse to map fields data.
    */
    public Map<String, String> parseMapFieldsData() {
    
        Map<String, String> mapResults = new Map<String, String>();
        
        for (AMC_Case_Merge_Settings__c item :AMC_Case_Merge_Settings__c.getAll().values()) {
            
            if (item.APIName__c == AMC_CASE_NUMBER || item.APIName__c == AMC_CREATE_DATE 
                || item.APIName__c == AMC_LAST_MOFIFIED_DATE || item.APIName__c == AMC_CASE_ID 
                || item.APIName__c == AMC_CREATE_BY || item.APIName__c == AMC_LAST_MOFIFIED_BY ) {
            
                continue;
            }
            mapResults.put(item.APIName__c, item.Name);
        }
        
        return mapResults;
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 07
    * @desciption: handle action merge case.
    */
    public PageReference actionMerge() {
    
        System.debug('[AMC_MergeCaseCtrl] - actionMerge - final case id: ' + this.finalCaseId );
        
        System.debug('[AMC_MergeCaseCtrl] - actionMerge - temporary case object: ' + this.finalCase );
        
        Savepoint sp = Database.setSavepoint();
        
        Case masterCase = null;
        
        try {
        
            //get master records from final case id.
            for (Integer idx = 0; idx < this.listCase.size(); idx ++) {
            
                if (this.listCase[idx].Id == this.finalCaseId) {
                
                    masterCase = this.listCase.remove(idx);
                    
                    break;
                }
            }
            
            //refresh str list id.
            String newStrListId = this.strListId.replace(this.finalCaseId, '').replace(',,', ',');
            
            if (newStrListId.subString(0,1) == ',') {
            
                newStrListId = newStrListId.subString(1, newStrListId.length());
            }
            
            if (newStrListId.subString(newStrListId.length() -1 , newStrListId.length()) == ',') {
            
                newStrListId = newStrListId.subString(0, newStrListId.length() -1);
            }
            
            //apply all fields selected for merge into master records.
            if (masterCase == null) {
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.AMC_ERR_MERGE_CASE_NO_DETECT_MASTER_RECORD));
                
                return null;
            }
            
            for (String item : this.mapFields.keySet()) {
                
                if (this.mapRowCaseDiff.get(item) == true && checkUpdateAvailableForField(item, 'Case')) {
                    
                    masterCase.put(item, this.finalCase.get(item));
                }
            }
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - master case object after merged: ' + masterCase);
            
            //System.debug('[AMC_MergeCaseCtrl] - actionMerge - related obj: ' + getChildRelationShipCustomObj());
        
            //build the query string to retrive all child related obj.
            String queryStr ='SELECT Id ';
            
            Map<String, String> mapObjRelatedToCase = getChildRelationShipCustomObj();
            
            for (String key : mapObjRelatedToCase.keyset()) {
            
                queryStr += ', (SELECT ' + mapObjRelatedToCase.get(key) + ' FROM ' + key +')' ;
            }
            
            queryStr += ' FROM Case WHERE Id IN (\'' + newStrListId.replace(',' , '\',\'') + '\')';
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - the query str to retrive related obj: ' + queryStr);
            
            //create list sobject to store number records related to merge.
            List<Sobject> listRelatedMerge = new List<Sobject>();
            
            for (Case item : Database.query(queryStr)) {
            
                for (String key : mapObjRelatedToCase.keyset()) {
                
                    String childField = mapObjRelatedToCase.get(key);
                    
                    //System.debug('[AMC_MergeCaseCtrl] - actionMerge - related obj: ' + key );
                    
                    //System.debug('[AMC_MergeCaseCtrl] - actionMerge - related field of obj: ' + childField );
                
                    if (null != item.getsObjects(key)) {
                        
                        for (SObject relatedItem : item.getsObjects(key)) {
                        
                            //System.debug('[AMC_MergeCaseCtrl] - actionMerge - related field id before: ' + relatedItem.get(childField));
                            
                            System.debug('[AMC_MergeCaseCtrl] - actionMerge - object " ' + relatedItem.getSObjectType().getDescribe().getName() 
                                            + '" with field "' + childField + '" :' + checkUpdateAvailableForField(childField, relatedItem.getSObjectType().getDescribe().getName()));
                            
                            //if (checkUpdateAvailableForField(childField, relatedItem.getSObjectType().getDescribe().getName())) {
                            
                                System.debug('[AMC_MergeCaseCtrl] - actionMerge - related field id can be updated: '+ relatedItem.getSObjectType().getDescribe().getName());
                                
                                relatedItem.put(childField ,this.finalCaseId);
                                
                                listRelatedMerge.add(relatedItem);
                            //}
                            
                            //System.debug('[AMC_MergeCaseCtrl] - actionMerge - related field id after: ' + relatedItem.get(childField));
                        }
                    }
                }
            }
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - HEAP SIZE used after change = ' + System.Limits.getHeapSize());
            
            //####### make clone standard object related with case without update parent field id.####
            List<Sobject> listRelatedClone = new List<Sobject>();
            
            //clone activity history.
            /*
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('ActivityHistory') 
                    + ' FROM ActivityHistory WHERE WhatId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('WhatId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            */
            
            //clone open activity.
            /*
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('OpenActivity') 
                    + ' FROM OpenActivity WHERE WhatId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('WhatId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            */
            
            //clone FeedComment.
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - FeedComment fields: ' + AMC_Utils.getAllObjectApiFieldInString('FeedComment'));
            
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('FeedComment') 
                    + ' FROM FeedComment WHERE ParentId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                //System.debug('feed comment: ' + item);
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('ParentId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            
            //clone FeedItem.
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - feed item fields: ' + AMC_Utils.getAllObjectApiFieldInString('FeedItem'));
            
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('FeedItem') 
                    + ' FROM FeedItem WHERE ParentId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                //check type is trackedChange or not. if yes, ignore.
                if (item.get('Type') == 'TrackedChange') {
                
                    continue;
                }
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('ParentId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            
            //clone EmailMessage.
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - EmailMessage fields: ' + AMC_Utils.getAllObjectApiFieldInString('EmailMessage'));
            
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('EmailMessage') 
                    + ' FROM EmailMessage WHERE ParentId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('ParentId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            
            //clone CaseComment.
            for (sObject item : Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('CaseComment') 
                    + ' FROM CaseComment WHERE ParentId IN (\'' + newStrListId.replace(',' , '\',\'') + '\')')) {
                
                sObject cloneItem = item.clone(false,true,false,false);
                
                cloneItem.put('ParentId', this.finalCaseId);
                
                listRelatedClone.add(cloneItem);
            }
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - HEAP SIZE used after clone = ' + System.Limits.getHeapSize());
            
            
            //########################## process merge into database and commit them. ###########################################
            update masterCase;
            
            if (listRelatedMerge.isEmpty() == false) {
                
                update listRelatedMerge;
            }
            
            if (listRelatedClone.isEmpty() == false) {
            
                insert listRelatedClone;
            }
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - list clone = ' + listRelatedClone);
            
            
            delete this.listCase;
            
            //for test uncomment this line.
            //Database.rollback(sp);
            
            return new PageReference('/' + masterCase.id);
           
        }
        catch (Exception e) {
        
            Database.rollback(sp);
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            
            System.debug('[AMC_MergeCaseCtrl] - actionMerge - ERROR: ' + e.getMessage());
            
            String[]pListId = this.strListId.split(',');
            
            this.listCase = Database.query('SELECT ' + AMC_Utils.getAllObjectApiFieldInString('Case') 
                                                + ' FROM Case WHERE Id IN :pListId ORDER BY CreatedDate ASC');
        }
        
        return null;
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 07
    * @desciption: handle action cancel to back the previous url.
    */
    public PageReference actionCancel() {
    
        String url = ApexPages.currentPage().getParameters().get('burl');
        
        System.debug('[AMC_MergeCaseCtrl] - actionCancel - back url parameter : ' + url);
        
        if (String.isBlank(url)) {
        
            url = System.URL.getSalesforceBaseUrl().toExternalForm() +'/500?nooverride=1';
        }
        
        System.debug('[AMC_MergeCaseCtrl] - actionCancel - back url: ' + url);
        
        return new PageReference(url);
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 06
    * @desciption: detect fields differece with others.
    */
    public Map<String, Boolean> buildListCaseDifference(List<Case> pListCase, Set<String> pSetApiField) {
    
        Map<String, Boolean> mapArrResults = new Map<String, Boolean>();
        
        if (pListCase.isEmpty() == false) {
        
            for (String item : pSetApiField) {
            
                mapArrResults.put(item, false);
            }
            
            Case tempCa = pListCase.get(0);
            
            for (Integer i= 1 ;i < pListCase.size() ; i++) {
            
                Case item = pListCase.get(i);
            
                for (String fieldName : pSetApiField) {
            
                    if (item.get(fieldName) != tempCa.get(fieldName)) {
                    
                        mapArrResults.put(fieldName, true);
                    }
                }   
            }
            
        }
        
        System.debug('[AMC_MERGE_CASE_PAGE] -buildListCaseDifference : mapArrRessults: ' + mapArrResults);
        
        return mapArrResults;
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 08
    * @desciption: check field availabel for update.
    */
    public boolean checkUpdateAvailableForField(String pApiField, String objName) {
    
        SObjectType objType= Schema.getGlobalDescribe().get(objName);

        Map<String, Schema.SObjectField> mapField = objType.getDescribe().fields.getMap();
        
        for(String fieldName : mapField.keySet()) {
        
           if (pApiField == fieldName && mapField.get(fieldName).getDescribe().isUpdateable()) {
           
               return true;
           }
        }
        
        return false;
    }
    
    /**
    * @author: CuongPQ.
    * @date: 2016 Sep 08
    * @desciption: detect these object related with case object.
    */
    public Map<String, String> getChildRelationShipCustomObj() {
    
        Map<String, String> results = new Map<String,String>();
        
        Schema.DescribeSObjectResult desObjResults = Case.SObjectType.getDescribe();
        
        List<Schema.ChildRelationship> childRelationship = desObjResults.getChildRelationships();
        
        for (Schema.ChildRelationship item : childRelationship) {
            
            /*
            //detect custom related child.
            if (null!= item.getRelationshipName() && item.getRelationshipName().contains('__r') == true ) {
             
                results.put(item.getRelationshipName(), item.getField().getDescribe().getName());
            }
            //detect standard child provided.
            else if (null != item.getChildSObject() && mapObjRelated.containsKey(item.getChildSObject().getDescribe().getName()) == true) {
            
                results.put(item.getRelationshipName(), item.getField().getDescribe().getName());
            }
            */
            
            //get all related child from case.-- note maximum 20 child.
            
            if (null != item.getChildSObject() && null != item.getRelationshipName() 
                && checkUpdateAvailableForField(item.getField().getDescribe().getName(), item.getChildSObject().getDescribe().getName())) {
            
                results.put(item.getRelationshipName(), item.getField().getDescribe().getName());
            }
            
        }
        
        return results;
    }
}